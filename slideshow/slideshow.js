createSlideshow()

function createSlideshow() {
    var head_tag = document.head;
    var body_tag = document.body;

    // appending a div(slideshow container) into body element of a webpage which has previous and next buttons to view the slideshow.
    var container = document.createElement('div');
    container.id = 'slideshow-container';
    body_tag.appendChild(container);

    // injecting html code
    container.innerHTML = "<div class='w3-center'>"+
      "<div class='w3-section'>"+
         "<button class='w3-button w3-orange' onclick='plusDivs(-1)'>❮ Prev</button>"+"&nbsp;"+
         "<button class='w3-button w3-orange' onclick='plusDivs(1)'>Next ❯</button>"+
      "</div>"+ 
    "</div>"

    // appending slideshow js script into body element of a webpage
    var script_tag = document.createElement('script');
    script_tag.type = 'text/javascript';
    script_tag.src = 'https://gl.githack.com/sadhanavirupaksha/page-xfrmtns-exps/raw/master/slideshow/w3.js';
    body_tag.appendChild(script_tag);
    
    // appending a CSS stylesheet into head element of a webpage, which is used to stylize the view of the container.
    var container_linktag = document.createElement('link');
    container_linktag.rel = 'stylesheet';
    container_linktag.type = 'text/css';
    container_linktag.href = 'https://gl.githack.com/sadhanavirupaksha/page-xfrmtns-exps/raw/master/slideshow/slideshow.css'; 
    head_tag.appendChild(container_linktag);

    // appending stylesheet into head element of a webpage
    var link_tag = document.createElement('link');
    link_tag.rel = 'stylesheet';
    link_tag.href = 'https://www.w3schools.com/w3css/4/w3.css';
    head_tag.appendChild(link_tag);

    // appending class attributes to body elements to make a slideshow out of it.
    var body_children = document.body.children;
    for (var i = 0; i < body_children.length; i++) {
    	if(body_children[i].tagName == 'SCRIPT') {
	    console.log(body_children[i]);
    	}
	else if(body_children[i].id == 'slideshow-container'){
    	    console.log(body_children[i]);
    	}
	else {
	    body_children[i].className += ' mySlides w3-container  w3-white w3-card-4';
	}
    }
}



