createFlipbook();

function createFlipbook() {
    var head_tag = document.head;
    var body_tag = document.body;

    // appending stylesheet into head element of a webpage which is used to style the flipbook
    var link_tag = document.createElement('link');
    link_tag.rel = 'stylesheet';
    link_tag.href = 'https://gl.githack.com/sadhanavirupaksha/page-xfrmtns-exps/raw/master/flipbook/flipbook.css';
    head_tag.appendChild(link_tag);

    // appending jquery script into head element of a webpage
    var jq_script = document.createElement('script');
    jq_script.type = 'text/javascript';
    jq_script.src = 'https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js';
    head_tag.appendChild(jq_script);

    // appending turn js script into head element of a webpage
    var turnjs_lib = document.createElement('script');
    turnjs_lib.type = 'text/javascript';
    turnjs_lib.src = 'https://gl.githack.com/sadhanavirupaksha/page-xfrmtns-exps/raw/master/flipbook/turn.min.js';
    head_tag.appendChild(turnjs_lib);

    // appending turn js function into body element of a webpage which is used to flip the book.
    var turnjs_func = document.createElement('script');
    turnjs_func.type = 'text/javascript';
    turnjs_func.src = 'https://gl.githack.com/sadhanavirupaksha/page-xfrmtns-exps/raw/master/flipbook/flipbook.js';
    body_tag.appendChild(turnjs_func);

    // Extract the body content of a webpage
    var body_content= body_tag.innerHTML;
    console.log(typeof body_content);
    
    // hierarchy of a flipbook
    var hierarchy = "<div id='flipbook'>"+
                       "<div class='hard'>Flip the Book</div>"+
                       "<div class='hard'></div>"+body_content+"<div class='hard'></div>"+
                       "<div class='hard'></div>"+
                    "</div>";
    alert(hierarchy);

    body_tag.innerHTML = hierarchy;
}
